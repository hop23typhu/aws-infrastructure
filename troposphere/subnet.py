from troposphere import (
    AWS_REGION, GetAZs, Join, Output, Parameter, Ref, Select, Tags, Template
)
from troposphere.constants import STRING, VPC_ID
from troposphere.ec2 import (
    NetworkAcl, NetworkAclEntry, PortRange, Route, RouteTable, Subnet,
    SubnetNetworkAclAssociation, SubnetRouteTableAssociation
)

template = Template()

environment_name = template.add_parameter(
  Parameter('Environment',Type=STRING)
)

cidr_block_public = template.add_parameter(
  Parameter(
    'CidrBlockPublic',
    Type= STRING,
  )
)

cidr_block_private = template.add_parameter(
  Parameter(
    'CidrBlockPrivate',
    Type=STRING,
    
  )
)

vpc_id = template.add_parameter(
  Parameter(
    'VpcId',
    Type= VPC_ID
  )
)

internet_gateway_id = template.add_parameter(
  Parameter(
    'InternetGatewayId',
    Type=STRING
  )
)

subnet_public = template.add_resource(
  Subnet(
    'SubnetPublic',
    AvailabilityZone=Select(0,GetAZs(Ref(AWS_REGION))),
    CidrBlock=Ref(cidr_block_public),
    MapPublicIpOnLaunch=True,
    Tags= Tags(Name=Join('-',[
      Ref(environment_name),
      'public-subnet-az'
    ])),
    VpcId=Ref(vpc_id)
  )
)

subnet_private = template.add_resource(
  Subnet(
    'SubnetPrivate',
    AvailabilityZone=Select(0,GetAZs(Ref(AWS_REGION))),
    CidrBlock=Ref(cidr_block_private),
    MapPublicIpOnLaunch=True,
    Tags= Tags(Name=Join('-',[
      Ref(environment_name),
      'private-subnet-az'
    ])),
    VpcId=Ref(vpc_id)
  )
)


route_table = template.add_resource(
 RouteTable(
   'RouteTable',
   Tags= Tags(Name=Join('-',[Ref(environment_name),'public-rtb'])),
   VpcId=Ref(vpc_id)
 )
)


template.add_resource(
  [
    Route(
      'InternetGatewayRoute',
      DestinationCidrBlock='0.0.0.0/0',
      GatewayId=Ref(internet_gateway_id),
      RouteTableId=Ref(route_table)
    ),
    SubnetRouteTableAssociation(
      'SubnetRouteTableAssociationPublic',
      RouteTableId=Ref(route_table),
      SubnetId= Ref(subnet_public)
    ),
    SubnetRouteTableAssociation(
      'SubnetRouteTableAssociationPrivate',
      RouteTableId=Ref(route_table),
      SubnetId= Ref(subnet_private)
    )
  ]
)

network_acl = template.add_resource(
  NetworkAcl(
    'NetworkAcl',
    Tags=Tags(Name=Join('-',[Ref(environment_name), 'public-acl'])),
    VpcId=Ref(vpc_id)
  ),
  
)

template.add_resource(
  [
    NetworkAclEntry(
      'EgressNetworkAclEntry',
      CidrBlock='0.0.0.0/0',
      Egress=True,
      NetworkAclId=Ref(network_acl),
      PortRange=PortRange(From=0,To=65535),
      Protocol=-1,
      RuleAction='allow',
      RuleNumber=100
    ),
    NetworkAclEntry(
          'IngressNetworkAclEntry',
          CidrBlock='0.0.0.0/0',
          Egress=False,
          NetworkAclId=Ref(network_acl),
          PortRange=PortRange(From=0, To=65535),
          Protocol=-1,
          RuleAction='allow',
          RuleNumber=100
    ),
    SubnetNetworkAclAssociation(
          'SubnetNetworkAclAssociationPublic',
          SubnetId=Ref(subnet_public),
          NetworkAclId=Ref(network_acl)
    ),
    SubnetNetworkAclAssociation(
          'SubnetNetworkAclAssociationPrivate',
          SubnetId=Ref(subnet_private),
          NetworkAclId=Ref(network_acl)
    )
  ]
)

template.add_output([
    Output('SubnetIdPublic',Value=Ref(subnet_public)),
    Output('SubnetIdPrivate',Value=Ref(subnet_private)),
    Output('SubnetCidrBlockPublic',Value=Ref(cidr_block_public)),
    Output('SubnetCidrBlockPrivate',Value=Ref(cidr_block_private)),
    Output('RouteTableId',Value=Ref(route_table))
])

print(template.to_json())