from troposphere import (
  AWS_REGION, Join, Output, Parameter, Ref, Tags, Template
)
from troposphere.constants import STRING, VPC_ID
from troposphere.route53 import (
  HostedZone, HostedZoneConfiguration, HostedZoneVPCs
)

template = Template()

environment_name = template.add_parameter(
  Parameter('EnvironmentName', Type=STRING)
)

hosted_zone_name = template.add_parameter(
  Parameter('HostedZoneName', Type=STRING)
)

vpc_id = template.add_parameter(
  Parameter('VpcId', Type=VPC_ID)
)

hosted_zone = template.add_resource(
  HostedZone(
    'HostedZone',
    HostedZoneConfig = HostedZoneConfiguration(Comment=Join('.',[
      Ref(environment_name),
      'server-hostedzone'
    ])),
    HostedZoneTags= Tags(Name=Join('.',[
      Ref(environment_name),
      'server-hostedzone'
    ])),
    Name=Ref(hosted_zone_name),
    VPCs = [
      HostedZoneVPCs(VPCId=Ref(vpc_id), VPCRegion = Ref(AWS_REGION))
    ]
  )
)

template.add_output([
  Output('HostedZoneId', Value = Ref(hosted_zone)),
  Output('HostedZoneName',Value = Ref(hosted_zone_name))
])

print(template.to_json())