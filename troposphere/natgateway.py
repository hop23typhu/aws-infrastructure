from troposphere import GetAtt, Output, Parameter, Ref, Template
from troposphere.constants import SUBNET_ID
from troposphere.ec2 import EIP, NatGateway

template = Template()

subnet_id_public = template.add_parameter(
  Parameter(
    'SubnetIdPublic',
    Type= SUBNET_ID
  )
)

subnet_id_private = template.add_parameter(
  Parameter(
    'SubnetIdPrivate',
    Type= SUBNET_ID
  )
)

eip_public  = template.add_resource(
  EIP("EIPPUBLIC", Domain="vpc")
)

eip_private  = template.add_resource(
  EIP("EIPPRIVATE", Domain="vpc")
)

nat_gateway_public = template.add_resource(
  NatGateway(
    'NatGatewayPublic',
    AllocationId = GetAtt(eip_private, 'AllocationId'),
    SubnetId = Ref(subnet_id_private)
  )
)

nat_gateway_private = template.add_resource(
  NatGateway(
    'NatGatewayPrivate',
    AllocationId = GetAtt(eip_public, 'AllocationId'),
    SubnetId = Ref(subnet_id_public)
  )
)


template.add_output([
  Output("NatGatewayIdPublic", Value=Ref(nat_gateway_public)),
  Output("NatGatewayIdPrivate", Value=Ref(nat_gateway_private))
])

print(template.to_json())