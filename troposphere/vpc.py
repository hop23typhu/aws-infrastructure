from troposphere import Join, Output, Parameter, Ref, Tags, Template
from troposphere.constants import STRING
from troposphere.ec2 import InternetGateway, VPC, VPCGatewayAttachment

template = Template()

environment_name = template.add_parameter(
  Parameter('EnvironmentName',Type=STRING)
)

cidr_block = template.add_parameter(
  Parameter(
    'CidrBlock',
    Type=STRING
  )
)

vpc = template.add_resource(
  VPC(
    'Vpc',
    CidrBlock=Ref(cidr_block),
    EnableDnsSupport=True,
    EnableDnsHostnames=True,
    Tags=Tags(Name=Join('-', [Ref(environment_name), 'vpc']))
  )
)

internet_gateway = template.add_resource(
  InternetGateway(
    'InternetGatgway',
    Tags=Tags(Name=Join('-',[Ref('environment'),'igw']))
  )
)

template.add_resource(
  VPCGatewayAttachment(
    'VPCGatewayAttachment',
    VpcId=Ref(vpc),
    InternetGatewayId=Ref(internet_gateway)
  )
)

template.add_output([
  Output('VpcId',Value=Ref(vpc)),
  Output('VpcCidrBlock', Value=Ref(cidr_block)),
  Output('InternetGatewayId',Value=Ref(internet_gateway))
])

print(template.to_json())