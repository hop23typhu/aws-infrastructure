from troposphere import (
    AWS_REGION, Equals, FindInMap, If, Join, Parameter, Ref, Template
)
from troposphere.constants import STRING, HOSTED_ZONE_ID
from troposphere.route53 import RecordSet, RecordSetType

template = Template()
environment_name = template.add_parameter(
    Parameter('EnvironmentName', Type=STRING)
)
hosted_zone_id = template.add_parameter(
    Parameter('HostedZoneId', Type=HOSTED_ZONE_ID)
)
domain_name = template.add_parameter(Parameter('DomainName', Type=STRING))
public_ip_0 = template.add_parameter(Parameter('PublicIp0', Type=STRING))
public_ip_1 = template.add_parameter(Parameter('PublicIp1', Type=STRING))
template.add_condition(
    'ProductionEnvironment', Equals(Ref(environment_name), 'prd')
)
template.add_resource(
    RecordSetType(
        'RecordSetType',
        Comment=Join('-', [Ref(environment_name), 'bastion-recordsettype']),
        HostedZoneId=Ref(hosted_zone_id),
        Name=If(
            'ProductionEnvironment',
            Join('.', ['bastion', Ref(domain_name)]),
            Join('', [
                'bastion-',
                Ref(environment_name),
                '.',
                Ref(domain_name)
            ])
        ),
        ResourceRecords=[Ref(public_ip_0), Ref(public_ip_1)],
        TTL='300',
        Type='A'
    )
)
print(template.to_json())
