from awacs.helpers.trust import get_default_assumerole_policy
from troposphere import (
    AWS_REGION, Base64, FindInMap, GetAtt, Join, Output, Parameter, Ref, Tags,
    Template
)
from troposphere.constants import STRING, SUBNET_ID, VPC_ID
from troposphere.ec2 import (
    Instance, NetworkInterfaceProperty, SecurityGroup, SecurityGroupRule
)
from troposphere.iam import InstanceProfile, Policy, Role
from troposphere.logs import LogGroup

template = Template()
environment_name = template.add_parameter(
    Parameter('EnvironmentName', Type=STRING)
)
policy_document = template.add_parameter(
    Parameter('PolicyDocument', Type=STRING)
)
vpc_id = template.add_parameter(Parameter('VpcId', Type=VPC_ID))
key_name = template.add_parameter(Parameter('KeyName', Type=STRING))
instance_type = template.add_parameter(
    Parameter('InstanceType', Type=STRING)
)
user_data = template.add_parameter(Parameter('UserData', Type=STRING))
subnet_id_public = template.add_parameter(Parameter('SubnetIdPublic', Type=SUBNET_ID))
subnet_id_private = template.add_parameter(Parameter('SubnetIdPrivate', Type=SUBNET_ID))
# https://aws.amazon.com/amazon-linux-ami/
# 2017.03.1
template.add_mapping('RegionImageIdMap', {
    'us-east-1': {
        'nat': 'ami-d4c5efc2'
    }
})
role = template.add_resource(
    Role(
        'Role',
        AssumeRolePolicyDocument=get_default_assumerole_policy(),
        Path='/',
        Policies=[
            Policy(
                PolicyDocument=Ref(policy_document),
                PolicyName=Join('-', [
                    Ref(AWS_REGION),
                    Ref(environment_name),
                    'bastion-instance-policy'
                ])
            )
        ],
        RoleName=Join('-', [
            Ref(AWS_REGION),
            Ref(environment_name),
            'bastion-instance-role'
        ])
    )
)
instance_profile = template.add_resource(
    InstanceProfile(
        'InstanceProfile',
        Path='/',
        Roles=[Ref(role)]
    )
)
security_group = template.add_resource(
    SecurityGroup(
        'SecurityGroup',
        GroupDescription=Join('-', [Ref(environment_name), 'bastion-sg']),
        SecurityGroupEgress=[
            SecurityGroupRule(
                'EgressSecurityGroupRule',
                CidrIp='0.0.0.0/0',
                IpProtocol='-1'
            )
        ],
        SecurityGroupIngress=[
            SecurityGroupRule(
                'IngressSecurityGroupRule',
                CidrIp='0.0.0.0/0',
                IpProtocol='-1'
            )
        ],
        Tags=Tags(
            Name=Join('-', [Ref(environment_name), 'bastion-sg'])
        ),
        VpcId=Ref(vpc_id)
    )
)
template.add_resource(
    LogGroup(
        'LogGroup',
        LogGroupName=Join('-', [Ref(environment_name), 'bastion']),
        RetentionInDays=7
    )
)
instance_public = template.add_resource(
    Instance(
        'InstancePublic',
        IamInstanceProfile=Ref(instance_profile),
        ImageId=FindInMap('RegionImageIdMap', Ref(AWS_REGION), 'nat'),
        InstanceInitiatedShutdownBehavior='terminate',
        InstanceType=Ref(instance_type),
        KeyName=Ref(key_name),
        NetworkInterfaces=[
            NetworkInterfaceProperty(
                AssociatePublicIpAddress=True,
                DeviceIndex='0',
                SubnetId=Ref(subnet_id_public)
            )
        ],
        SourceDestCheck=True,
        Tags=Tags(Name=Join('-', [Ref(environment_name), 'bastion-az0'])),
        UserData=Base64(Ref(user_data))
    )
)
instance_private = template.add_resource(
    Instance(
        'InstancePrivate',
        IamInstanceProfile=Ref(instance_profile),
        ImageId=FindInMap('RegionImageIdMap', Ref(AWS_REGION), 'nat'),
        InstanceInitiatedShutdownBehavior='terminate',
        InstanceType=Ref(instance_type),
        KeyName=Ref(key_name),
        NetworkInterfaces=[
            NetworkInterfaceProperty(
                AssociatePublicIpAddress=True,
                DeviceIndex='0',
                SubnetId=Ref(subnet_id_private)
            )
        ],
        SourceDestCheck=True,
        Tags=Tags(Name=Join('-', [Ref(environment_name), 'bastion-az1'])),
        UserData=Base64(Ref(user_data))
    )
)
template.add_output([
    Output('InstancePublicIpPublic', Value=GetAtt(instance_public, 'PublicIp')),
    Output('InstancePublicIpPrivate', Value=GetAtt(instance_private, 'PublicIp'))
])
print(template.to_json())
