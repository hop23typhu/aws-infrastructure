#!/usr/bin/env bash

set -ex
SRC_DIR=$(dirname "$(realpath "$0")")
DST_DIR=$(realpath "${SRC_DIR}"/../ansible/files)
for FILE in ${SRC_DIR}/*.py
do
  python "${FILE}" > "${DST_DIR}"/"$(basename "${FILE}" .py)".json
done
