from troposphere import AWS_REGION, Join, Output, Parameter, Ref, Template
from troposphere.constants import VPC_ID
from troposphere.ec2 import VPCEndpoint

template = Template()

vpc_id = template.add_parameter(
  Parameter(
    'VpcId',
    Type=VPC_ID
  )
)

vpc_endpoint = template.add_resource(
  VPCEndpoint(
    'VPCEndpoint',
    ServiceName=Join('.',['com.amazonaws',Ref(AWS_REGION),'s3']),
    VpcId=Ref(vpc_id)
  )
)

template.add_output(
  Output('VpcEndpointId',Value=Ref(vpc_endpoint))
)

print(template.to_json())