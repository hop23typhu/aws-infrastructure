from troposphere import (
    GetAtt, Join, Output, Parameter, Ref, Tags, Template
)
from troposphere.constants import STRING
from troposphere.s3 import Bucket, BucketPolicy

template = Template()
environment_name = template.add_parameter(
    Parameter('EnvironmentName', Type=STRING)
)
policy_document = template.add_parameter(
    Parameter('PolicyDocument', Type=STRING)
)
bucket = template.add_resource(
    Bucket(
        'Bucket',
        BucketName=Join('-', [
            'kinkan',
            Ref(environment_name),
            'infrastructure-s3'
        ]),
        Tags=Tags(Name=Join('-', [
            'kinkan',
            Ref(environment_name),
            'infrastructure-s3'
        ]))
    )
)
template.add_resource(
    BucketPolicy(
        'BucketPolicy',
        Bucket=Ref(bucket),
        PolicyDocument=Ref(policy_document)
    )
)
template.add_output(
    Output('BucketDomainName', Value=GetAtt(bucket, 'DomainName'))
)
print(template.to_json())
