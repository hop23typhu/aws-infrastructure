#!/usr/bin/env bash

if [[ ! $1 ]]
then
  echo "Usage: $0 <env>"
  exit 1
fi
ENV=$1
cat <<EOF
#!/usr/bin/env bash

yum install -y awslogs
cat <<AWSLOGS > /etc/awslogs/awslogs.conf
[general]
state_file = /var/awslogs/state/agent-state

[dmesg]
log_group_name = ${ENV}-bastion
log_stream_name = dmesg_{instance_id}_{hostname}
file = /var/log/dmesg

[cloud-init]
log_group_name = ${ENV}-bastion
log_stream_name = cloud-init_{instance_id}_{hostname}
file = /var/log/cloud-init-output.log

[yum]
log_group_name = ${ENV}-bastion
log_stream_name = yum_{instance_id}_{hostname}
datetime_format = %Y-%m-%d %H:%M:%S
file = /var/log/yum.log

[messages]
log_group_name = ${ENV}-bastion
log_stream_name = messages_{instance_id}_{hostname}
datetime_format = %b %d %H:%M:%S
file = /var/log/messages

[secure]
log_group_name = ${ENV}-bastion
log_stream_name = secure_{instance_id}_{hostname}
datetime_format = %b %d %H:%M:%S
file = /var/log/secure

[audit]
log_group_name = ${ENV}-bastion
log_stream_name = audit_{instance_id}_{hostname}
datetime_format = %Y-%m-%d %H:%M:%S
file = /var/log/audit/audit.log

[script]
log_group_name = ${ENV}-bastion
log_stream_name = script_{instance_id}_{hostname}
datetime_format = %Y-%m-%d %H:%M:%S
file = /var/log/script.log
AWSLOGS
REGION=\$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone | sed -e "s/[a-z]$//")
sed -ie "s/us-east-1/${REGION}/g" /etc/awslogs/awscli.conf
mkdir -p /var/awslogs/state
service awslogs start
chkconfig awslogs on
touch /var/log/script.log
chown ec2-user:ec2-user /var/log/script.log
echo "exec script -aefq /var/log/script.log" >> /etc/profile
curl -fLSs https://rpm.nodesource.com/setup_8.x | bash -
yum install -y nodejs
yum install -y gcc-c++ make
cat <<MONGODB_REPO > /etc/yum.repos.d/mongodb-org-3.4.repo
[mongodb-org-3.4]
name=MongoDB Repository
enabled=1
baseurl=https://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/3.4/x86_64/
gpgcheck=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc
MONGODB_REPO
yum update -y
yum install -y mongodb-org-shell
yum install -y jq
EOF
