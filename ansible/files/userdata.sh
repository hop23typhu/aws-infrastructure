#!/usr/bin/env bash

if [[ ! $1 ]]
then
  echo "Usage: $0 <env>"
  exit 1
fi
ENV=$1
cat <<EOF
#!/bin/bash

yum update -y
yum install -y aws-cli
aws s3 cp s3://stacc-${ENV}-infrastructure-s3/init.sh init.sh
bash init.sh
EOF
