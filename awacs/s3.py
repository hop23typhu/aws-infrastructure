import argparse
from awacs.aws import (
    Allow, AWSPrincipal, Condition, Policy, Statement, StringEquals, Everybody
)
from awacs.s3 import Action, ARN
from boto3.session import Session
from string_utils import snake_case_to_camel

region = Session().region_name
parser = argparse.ArgumentParser(
    description='Generates infrastructure S3 IAM policy document'
)
parser.add_argument('-e', '--environment-name', required=True)
parser.add_argument('-v', '--vpc-endpoint-id', required=True)
args = parser.parse_args()
environment_name = args.environment_name
policy_id = snake_case_to_camel('-'.join([
    region,
    environment_name,
    'infrastructure-s3-policy-document'
]), separator='-')
vpc_endpoint_id = args.vpc_endpoint_id
statement_id = snake_case_to_camel('-'.join([
    region,
    environment_name,
    'infrastructure-s3-statement'
]), separator='-')
policy = Policy(
    Id=policy_id,
    Statement=[
        Statement(
            Action=[Action('*')],
            Condition=Condition(
                StringEquals({'aws:SourceVpce': vpc_endpoint_id})
            ),
            Effect=Allow,
            Principal=AWSPrincipal('*'),
            Resource=[ARN('-'.join([
                'stacc',
                environment_name,
                'infrastructure-s3/*'
            ]))],
            Sid=statement_id
        )
    ]
)
print(policy.to_json())
