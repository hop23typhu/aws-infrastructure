import argparse
from awacs.aws import Action, Allow, Everybody, Policy, Statement
from boto3.session import Session
from string_utils import snake_case_to_camel

region = Session().region_name
parser = argparse.ArgumentParser(
    description='Generates NAT instance IAM policy document'
)
parser.add_argument('-e', '--environment-name', required=True)
args = parser.parse_args()
environment_name = args.environment_name
policy_id = snake_case_to_camel('-'.join([
    region,
    environment_name,
    'bastion-instance-policy-document'
]), separator='-')
statement_id = snake_case_to_camel('-'.join([
    region,
    environment_name,
    'bastion-instance-statement'
]), separator='-')
policy = Policy(
    Id=policy_id,
    Statement=[
        Statement(
            Action=[Action(Everybody)],
            Effect=Allow,
            Resource=[Everybody],
            Sid=statement_id
        )
    ]
)
print(policy.to_json())
