from troposphere import Ref, Template
import troposphere.ec2 as ec2

t = Template()
instance = ec2.Instance("NyInstance")
instance.ImageId = "ami-9534343"
instance.InstanceType = "t1.micro"

t.add_resource(instance)
print(t.to_json())