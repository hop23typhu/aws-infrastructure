from troposphere import GetAtt, Join, Output, Parameter, Ref, Template
from troposphere.ec2 import SecurityGroup
from troposphere.rds import DBInstance, DBSubnetGroup

t.Template()

t.add_description(
  'AWS CloudFormartion sample template VPC_RDS_DB_Instance'
)
vpcid = t.add_parameter(Parameter(
  "VpcId",
  Type=String,
  Description="VpcId of your existing virtual private cloud"
))

subnet = t.add_parameter(Parameter(
  "Subnet",
  Type="CommaDelimitedList",
  Description = (
    "The list of SubnetIds, for at least 2 AZ in the region in your VPC"
  )
))

dbname = t.add_parameter(Parameter(
  "DBName",
  Default= "MyDatabase",
  Description="The database name",
  Type= String,
  Minlength = "1",
  MaxLength = "64",
  AllowedPattern = "[a-zA-Z][a-zA-Z0-9]",
  ConstraintDescription=("must begin with a letter and contain only alphanumeric characters.")
))

