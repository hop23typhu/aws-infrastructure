# AWS trainning
Hello guys, now we start to build an infrastructure as code that I use AWS.
# 1, Libraries
- Troposphere
- AWACS
- Ansible
- AWSCLI
- Python

# 2, How to build infrastructure
## Step 1: Build structure project
```
Root_project
    - ansible
        - files
        - roles
            - bastion
                - files
                    - init.sh
                    - userdata.sh
                - tasks
                    - mail.yml
                - vars
                    - prd.yml
                    - dev.yml
            - vpc
                - files
                     - init.sh
                     - userdata.sh
                - tasks
                    - mail.yml
                - vars
                    - prd.yml
                    - dev.yml
            - ...
        - dev.yml
        - prd.yml
    - awacs
        - bastion.py
        - vpc.py
        - ...
    - troposphere
        - generate.sh
        - bastion.py
        - vpc.py
        - ...
    - requirements.txt
    - env.sh
    - README.md
```
### Folder troposphere
This folder contains python files that will generate to template file(json).
File generate is used to generate file json and save to ansible/files


## Step 2: Create template files
### a, What is file template format
A template has the following high level JSON structure:
```
{
    "Description" : "A text description for the template usage",
    "Parameters": {
        // A set of inputs used to customize the template per deployment
    },
    "Resources" : {
        // The set of AWS resources and relationships between them
    },
    "Outputs" : {
        // A set of values to be made visible to the stack creator
    },
    "AWSTemplateFormatVersion" : "2010-09-09"
}
```
The following template is a simple example that shows how to create an EC2 instance:
```
{
    "Description" : "Create an EC2 instance running the Amazon Linux 32 bit AMI.",
    "Parameters" : {
        "KeyPair" : {
            "Description" : "The EC2 Key Pair to allow SSH access to the instance",
            "Type" : "String"
        }
    },
    "Resources" : {
        "Ec2Instance" : {
            "Type" : "AWS::EC2::Instance",
            "Properties" : {
                "KeyName" : { "Ref" : "KeyPair" },
                "ImageId" : "ami-3b355a52"
            }
        }
    },
    "Outputs" : {
        "InstanceId" : {
            "Description" : "The InstanceId of the newly created EC2 instance",
            "Value" : {
                "Ref" : "Ec2Instance"
            }
        }
    },
    "AWSTemplateFormatVersion" : "2010-09-09"
}   
```
### b, How to create template file by Troposphere and awacs
#### What is Troposphere
The troposphere library allows for easier creation of the AWS CloudFormation JSON by writing Python code to describe the AWS resources. troposphere also includes some basic support for OpenStack resources via Heat.

#### How to install Troposphere
```
troposphere can be installed using the pip distribution system for Python by issuing:

$ pip install troposphere
To install troposphere with awacs (recommended soft dependency):

$ pip install troposphere[policy]
Alternatively, you can run use setup.py to install by cloning this repository and issuing:

$ python setup.py install  # you may need sudo depending on your python installation
```
#### How to create a simple template
A simple example to create an instance would look like this:
```
from troposphere import Ref, Template
import troposphere.ec2 as ec2
t = Template()
instance = ec2.Instance("myinstance")
instance.ImageId = "ami-951945d0"
instance.InstanceType = "t1.micro"
t.add_resource(instance)
print(t.to_json())
```

Result:
```
{
    "Resources": {
        "myinstance": {
            "Properties": {
                "ImageId": "ami-951945d0",
                "InstanceType": "t1.micro"
            },
            "Type": "AWS::EC2::Instance"
        }
    }
}
```
Alternatively, parameters can be used instead of properties:
```
instance = ec2.Instance("myinstance", ImageId="ami-951945d0", InstanceType="t1.micro")
t.add_resource(instance)
```
And add_resource() returns the object to make it easy to use with Ref():

```
instance = t.add_resource(ec2.Instance("myinstance", ImageId="ami-951945d0", InstanceType="t1.micro"))
Ref(instance)
```

To get more detail, please take a look this link https://github.com/cloudtools/troposphere

## Step 3: Create stack with ansible-playbook

Take a look this document http://docs.ansible.com/ansible/latest/playbooks_intro.html

An Ansible playbook is an organized unit of scripts that defines work for a server configuration managed by the automation tool Ansible.

Ansible is a configuration management tool that automates the configuration of multiple servers by the use of Ansible playbooks.  The playbook is the core component of any Ansible configuration. An Ansible playbook contains one or multiple plays, each of which define the work to be done for a configuration on a managed server. Ansible plays are written in YAML. Every play is created by an administrator with environment-specific parameters for the target machines; there are no standard plays.

The playbook is therefore composed of plays, which are composed of modules. It executes when the administrator runs the ansible-playbook command against target machines. The administrator must use an inventory file to specify the hosts under the playbook's management. The inventory file contains a list of all hosts that are managed by Ansible, and it offers an option to group hosts according to their functionality. For example, an administrator can apply a play to a group of web servers in the playbook, and a different play to a group of database servers.
```
- hosts: servertest 
  remote_user: root
  tasks:
  ########## install httpd and start .
  - name: Install HTTP
    yum: name=httpd state=latest
  - name: Start HTTPD after install
    service: name=httpd state=started
########### Deploy config
#backup
  - name: Backup config HTTP (backup from client)
    command: cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.backup1
#Deploy
  - name: Deploy config httpd
    template:
     src: "/etc/ansible/config/httpd.conf"
     dest: "/etc/httpd/conf/httpd.conf"
     owner: root
     group: root
     mode: 0644
########### Pull source to client
  - name: Deploy web file
    template:
     src: "/etc/ansible/config/index.html"
     dest: "/var/www/html/index.html"

########### Reset and apply config
  - name: Start HTTPD after install
    service: name=httpd state=restarted
```
